#pragma once

#include <glib.h>

extern gchar *term_name;

extern gchar **term_argv;
extern gint term_argc;

extern gchar *dbus_name;

extern gchar *hold_opt;

extern gboolean once_only;

void parse_options(int argc, char *argv[]);
