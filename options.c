#include "options.h"

gchar *term_name = NULL;

gchar **term_argv = NULL;
gint term_argc = 0;

gchar *dbus_name = NULL;

gchar *hold_opt = NULL;

gboolean once_only = FALSE;

static GOptionEntry entries[] = {
	{"dbus-name", 'd', 0, G_OPTION_ARG_STRING, &dbus_name, "Name to acquire on the session bus (required)", "NAME"},
	{"hold-opt", 'h', 0, G_OPTION_ARG_STRING, &hold_opt, "option to be used to keep the terminal open after the application terminates", "OPT"},
	{"once-only", 'o', 0, G_OPTION_ARG_NONE, &once_only, "Exit as soon as LaunchCommand is called once", NULL},
	{ NULL },
};


void parse_options(int argc, char *argv[]) {
	GError *error;
	gchar *help = NULL;
	GOptionContext *context = g_option_context_new("COMMAND ARGS...");
	g_option_context_set_strict_posix(context, TRUE);
	g_option_context_add_main_entries(context, entries, NULL);
	g_option_context_set_summary(context, "Implement org.freedeskto.Terminal1 interface to launch an app in an arbitrary terminal.");
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		help = g_option_context_get_help(context, FALSE, NULL);
		g_printerr("ERROR: option parsing failed: %s\n\n%s", error->message, help);
		goto error;
	}

	if (dbus_name == NULL) {
		help = g_option_context_get_help(context, FALSE, NULL);
		g_printerr("ERROR: dbus-name is required\n\n%s", help);
		goto error;
	}

	if (argc < 2) {
		help = g_option_context_get_help(context, TRUE, NULL);
		g_printerr("ERROR: terminal command is required\n\n%s", help);
		goto error;
	}
	g_option_context_free(context);

	// argv includes the executable name, which we want to exclude
	term_argc = argc-1;
	term_argv = argv+1;
	return;

error:
	g_free(help);
	g_option_context_free(context);
	exit(1);
}
