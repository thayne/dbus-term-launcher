#!/usr/bin/env python3

"""
Generate the .desktop and .service files for popular terminals
"""

import os
import sys

TERMINALS = {
    "alacritty": "--hold-opt=--hold alacritty -e",
    "kitty": "--hold-opt=--hold kitty",
    "urxvt": "--hold-opt=-hold urxvt -e",
    "xterm": "--hold-opt=-hold xterm -e",
    "wezterm": '"--hold-opt=--config=exit_behavior=\\"Hold\\"" wezterm -e',
}

BINDIR = os.getenv("BINDIR", "/usr/bin")

DESKTOP_TEMPLATE = """\
[DesktopEntry]
Type=Application
Name={name}
Terminal=false
Categories=System;TerminalEmulator;
DBusActivatable=true
Exec={bindir}/dbus-term-launcher "--dbus-name={name} {args}
Implements=org.freedesktop.Terminal1
OnlyShowIn=
"""

DBUS_TEMPLATE = """\
[D-BUS Service]
Name={name}
Exec={bindir}/dbus-term-launcher "--dbus-name={name}" {args}
"""


def gen_desktop(name, args):
    content = DESKTOP_TEMPLATE.format(bindir=BINDIR, name=name, args=args)
    with open("org.freedesktop.Terminal1.{}.desktop".format(name), "w") as f:
        f.write(content)


def gen_dbus(name, args):
    content = DBUS_TEMPLATE.format(bindir=BINDIR, name=name, args=args)
    with open("org.freedesktop.Terminal1.{}.service".format(name), "w") as f:
        f.write(content)


if __name__ == "__main__":
    term = sys.argv[1]

    args = TERMINALS[term]
    gen_desktop(term, args)
    gen_dbus(term, args)
