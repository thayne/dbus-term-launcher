#define _GNU_SOURCE
#include <gio/gio.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>

#include "options.h"
#include "terminal1.h"

static int exit_code = 0;

static char *name2path(char *name) {
	int len = strlen(name);
	char *path = malloc(len + 2);
	char *dst = path, *src = name;
	*dst++ = '/';
	do {
		if (*src == '.') {
			*dst = '/';
		} else {
			*dst = *src;
		}
		dst++;
	} while (*src++);
	return path;
}

// TODO: request adding this to glib
gchar ** g_environ_putenv(gchar **envp, gchar *entry, gboolean overwrite) {
	g_return_val_if_fail (entry != NULL, NULL);
	char *key_end = index(entry, '=');
	if (key_end == NULL) {
		return g_environ_unsetenv(envp, entry);
	}
	int key_len = key_end - entry;
	int i = 0;
	for (; envp[i]; i++) {
		if (strncmp(envp[i], entry, key_len)) {
			// We found a matching pair
			if (overwrite) {
				g_free(envp[i]);
				envp[i] = entry;
			} else {
				g_free(entry);
			}
			return envp;
		}
	}
	envp = g_renew(gchar *, envp, i + 1);
	envp[i-1] = entry;
	envp[i] = NULL;
	return envp;
}

gboolean launch_app(
		int argc,
		gchar **argv,
		gchar *working_dir,
		gchar **env,
		gboolean keep_open,
		GVariant *platform_data,
		GError **error) {

	gchar **curr = NULL;

	if (working_dir == NULL || *working_dir == '\0') {
		working_dir = getenv("HOME");
	}


	// build command
	gchar ** command = g_new(gchar *, term_argc + argc + (keep_open ? 2 : 1));
	// Move the argv for the "exec" command to the end of hte arguments, including the trailing NULL.
	curr = command;
	*curr++ = term_argv[0];
	if (keep_open) {
		*curr++ = strdup(hold_opt);
	}
	if (term_argc > 1) {
		memcpy(curr, term_argv+1, (term_argc - 1) * sizeof(char*));
	}
	curr += term_argc - 1;
	memcpy(curr, argv, argc * sizeof(char*));
	curr[argc] = NULL;

	char *startupid = NULL;
	g_variant_lookup(platform_data, "desktop-startup-id", "s", &startupid);

	if (startupid) {
		env = g_environ_setenv(env, "DESKTOP_STARTUP_ID", startupid, TRUE);
	}

	gboolean result = g_spawn_async(working_dir, command, env, G_SPAWN_DO_NOT_REAP_CHILD |G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, error);

	g_free(command);
	g_free(startupid);
	g_strfreev(env);

	return result;
}

void handle_launch_command(
		Terminal1 *interface,
		GDBusMethodInvocation *invocation,
		gchar **exec,
		gchar *working_dir,
		gchar *desktop_entry,
		gchar **env,
		GVariant *options,
		GVariant *platform_data,
		GMainLoop *loop) {

	gint argc = 0;
	gchar **argv = NULL;
	GError *error = NULL;
	gboolean keep_open = FALSE;

	gchar *default_command[2] = { 0 };

	if (*exec == NULL) {
		// if "exec" is empty default to using the shell
		argc = 1;
		argv = default_command;
		argv[0] = getenv("SHELL");
		if (!argv[0]) {
			argv[0] = "/bin/sh";
		}
		argv[0] = strdup(argv[0]);
	} else {
		argv = exec;
		argc = g_strv_length(exec);
	}
	if (hold_opt) {
		g_variant_lookup(options, "keep-terminal-open", "b", &keep_open);
	}

	gchar **envp = g_get_environ();
	for(gchar **entry = env; *entry; entry++) {
		envp = g_environ_putenv(envp, strdup(*entry), TRUE);
	}

	if (launch_app(argc, argv, working_dir, envp, keep_open, platform_data, &error)) {
		g_dbus_method_invocation_return_value(invocation, NULL);
	} else {
		exit_code = error->code;
		g_dbus_method_invocation_return_gerror(invocation, error);
	}

	if (once_only) {
		g_main_loop_quit(loop);
	}
}

void handle_activate(Application *interface, GDBusMethodInvocation *invocation, GVariant *platform_data) {
	GError *error = NULL;
	gchar *shell = getenv("SHELL");
	if (shell == NULL) {
		shell = "/bin/sh";
	}

	gchar **env = g_get_environ();
	if (launch_app(1, &shell, NULL, env, FALSE, platform_data, &error)) {
		g_dbus_method_invocation_return_value(invocation, NULL);
	} else {
		g_dbus_method_invocation_return_gerror(invocation, error);
	}
}


void on_bus_acquired(GDBusConnection *connection, const gchar *name, void *user_data) {
	GError *error = NULL;
	char *path = name2path(dbus_name);

	Terminal1 *term_interface = terminal1_skeleton_new();
	g_signal_connect(term_interface, "handle-launch-command", G_CALLBACK (handle_launch_command), user_data);

	Application *app_interface = application_skeleton_new();
	g_signal_connect(app_interface, "handle-activate", G_CALLBACK (handle_activate), user_data);

	GDBusObjectSkeleton *object = g_dbus_object_skeleton_new(path);
	g_dbus_object_skeleton_add_interface(object, G_DBUS_INTERFACE_SKELETON (term_interface));
	g_dbus_object_skeleton_add_interface(object, G_DBUS_INTERFACE_SKELETON (app_interface));

	g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON (term_interface),
			connection, path, &error);
	g_assert_no_error(error);

	g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON (app_interface), connection, path, &error);
	g_assert_no_error(error);

	free(path);
	g_object_unref(term_interface);
	g_object_unref(app_interface);
}

void on_name_acquired(GDBusConnection *connection, const gchar *name, void *user_data) {
	g_printerr("Acquired %s\n", name);
}

void on_name_lost(GDBusConnection *connection, const gchar *name, void *loop) {
	exit_code = 2;
	g_main_loop_quit(loop);
}

void on_destroy(void *p) {}

int main(int argc, char **argv) {
	parse_options(argc, argv);

	GMainLoop *loop = g_main_loop_new(NULL, FALSE);

	guint ownerid = g_bus_own_name(
			G_BUS_TYPE_SESSION,
			dbus_name,
			G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT | G_BUS_NAME_OWNER_FLAGS_REPLACE,
			on_bus_acquired,
			on_name_acquired,
			on_name_lost,
			loop,
			NULL);

	g_main_loop_run(loop);

	g_bus_unown_name(ownerid);
	g_main_loop_unref(loop);

	return exit_code;
}
